Battery Hub
===========

Resume
------

A multiple connector set for extend an alone battery pack.

List of components
------------------

 * Car lighter female connector (x1): https://www.aliexpress.com/item/4001213460950.html
 * Female type A USB recessed connector (1x): https://www.aliexpress.com/item/32824558226.html
 * 5.5mmx2.5mm Female DC Power Jack Plug (for output) (x3): https://www.aliexpress.com/item/32869308687.html
 * 5.5mmx2.1mm Female DC Power Jack Plug (for input) (x1): https://www.aliexpress.com/item/4001185716546.html
 * 3A 12V to 5V converter (1x): https://www.aliexpress.com/item/32331808882.html
 * M3x10 bolt and nut for fix 12V to 5V converter (2x)
 * Battery monitor (1x): https://www.amazon.es/gp/product/B07MJKFR1Q
 * 5x20 fuse holder (x1): https://www.aliexpress.com/item/32864479515.html
 * M2.6x12 screws (4x): https://www.aliexpress.com/item/32975410255.html 

